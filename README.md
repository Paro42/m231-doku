# Lernjournal 231 Alain

[Backup](backup.md)

[TOC]

## 29/08/2023

Heute haben wir Git aufgesetzt, sodass man sein Lernjournal darauf führen kann.

Wichtige commands um Änderungen zu übernehmen: 

- git commit README.md

- git push


Ausserdem habe ich ein Ablagekonzept für meine Daten Visualisiert in Draw.io und Excel.


## 05/09/2023

Heute haben wir uns in den Themen Datenschutz und Datensicherheit vertieft.

Ausserdem haben wir uns genauer angeschaut wie Git funktioniert.

### Anleitung zum übernehmen des Lernjournal auf git:

- git pull (um aktuelles File zu holen)

- notepad README.md (um Text zu schreiben)

- git commit README.md (um änderungen zu übernehmen)

- git push (um auf git zu pushen)


## 12/09/2023

Heute haben wir uns hauptsächlich mit dem Thema "Backup" befasst.

### Hot Backup:

Dynamisches Backup, das heisst die Daten werden gesichert, während sie in Anwendung also Online sind.

(+) Keine Ausfallzeiten	

(-) Korrupte Files (Daten werden kopiert, während sie gerade aktualisiert werden)


### Cold Backup 

Ist eine offline-Sicherung, während eines Cold Backups kann nichts an den Daten verändert werden. 
Bei diesem Backup wird nur eine einzige Sicherungsversion erstellt.

(-) Ausfallzeiten 

(+) Sicherste Form eines Backups


### Authentisierung

 – das Nachweisen einer Identität
Im Rahmen einer Authentisierung erbringt eine Person einen Beweis dafür, dass sie ist, wer sie zu sein vorgibt. Im Alltag geschieht dies z. B. durch die Vorlage des Personalausweises. In der IT wird hierfür häufig ein Passwort in Kombination mit einem Benutzernamen genutzt.

### Authentifizierung

 – die Prüfung des o. g. Identitätsnachweises auf seine Authentizität
Im Alltag geschieht dies z. B. durch die Prüfung des Personalausweis auf Urkundenfälschung und durch den Abgleich mit der Person. In der IT wird z. B. überprüft, ob die Kombination von Benutzernamen und Passwort im System existiert.

### Autorisierung 

– das Gewähren des Zugangs zu den Privilegien, welche der erfolgreich nachgewiesenen Identität zustehen
Im Alltag kann dies nach Vorlage des Personalausweises der Zugang zu einem Unternehmen sein, bei dem man als Gast angemeldet wurde. Aber: Vielleicht erhält man als Gast nur den Zugang zum Besprechungsraum, nicht aber zur Montagehalle. In der IT kann nach der Autorisierung in einem Benutzerkonto z. B. gearbeitet werden. Aber wenn dieses Konto nicht über Administratorenrechte verfügt, können z B. keine neuen Programme installiert werden.


### 2-Faktor-Authentifizierung

Die Zwei-Faktor-Authentifizierung (2FA) ist ein Sicherheitsverfahren, bei dem zwei verschiedene Identitätsnachweise erforderlich sind, um auf ein Konto oder System zuzugreifen. Typischerweise umfasst dies etwas, was der Benutzer weiß (z. B. ein Passwort) und etwas, was der Benutzer besitzt (z. B. ein Smartphone oder ein Hardware-Token). 2FA erhöht die Sicherheit erheblich, da es schwieriger wird, die Identität eines Benutzers zu stehlen oder zu hacken, da beide Faktoren benötigt werden.


### 3-Faktor-Authentifizierung

Die Drei-Faktor-Authentifizierung (3FA) ist ein erweitertes Sicherheitsverfahren, das drei verschiedene Identitätsnachweise erfordert, um auf ein Konto oder System zuzugreifen. Im Gegensatz zur herkömmlichen 2FA, die zwei Faktoren verwendet, integriert die 3FA einen zusätzlichen Identitätsnachweis, um die Sicherheit weiter zu erhöhen. Diese dritte Ebene kann beispielsweise biometrische Daten wie Fingerabdrücke, Gesichtserkennung oder ein physisches Sicherheitstoken sein, was es noch schwieriger macht, die Identität eines Benutzers zu fälschen oder zu kompromittieren.


### "Was ist die Problematik von Datenlöschungen über alle Archive und Backups?"


Herausforderungen auf technischer Ebene: Archiv- und Backup-Systeme sind in der Regel darauf ausgerichtet, Daten zu speichern und zu sichern. Die Durchführung einer Datenlöschung ist technisch anspruchsvoll, da es oftmals schwierig ist sicherzustellen, dass keine verbliebenen Datenspuren zurückbleiben.

Kosten und Zeitaufwand: Das gewissenhafte Löschen von Daten aus Archiven und Backups erfordert Ressourcen und zeitlichen Aufwand. Dies kann kostspielig sein und die betrieblichen Abläufe beeinträchtigen.

Gefahr der Datenwiederherstellung: Selbst wenn Daten aus primären Systemen gelöscht werden, können sie möglicherweise noch in Archiven oder Backups vorhanden sein. Sollte es gelingen, diese Daten wiederherzustellen, könnten sie in unbefugte Hände geraten.

Rechtliche Beweismittel: Die Eliminierung von Daten aus Archiven und Backups könnte die Möglichkeit der Aufbewahrung wichtiger Beweismittel in rechtlichen Auseinandersetzungen beeinträchtigen.

Fehler und menschliches Versagen: Bei komplexen Löschvorgängen besteht stets die Gefahr von Fehlern und menschlichem Versagen, die schwerwiegende Auswirkungen nach sich ziehen können.


### Impressum, Disclaimer, AGB 

Impressum: 

- Übersetzungen: Erscheinungsvermerk (DE), imprint (EN), les mentions légales  (FR), informazione legale (IT), información legal (SP)

- Definition: Das Wort "Impressum" bezieht sich auf eine rechtliche Anforderung auf Websites und gedruckten Medien, die Informationen über den Verantwortlichen für den Inhalt bereitstellen. Es enthält in der Regel Angaben wie den Namen des Verantwortlichen, die Kontaktinformationen und rechtliche Hinweise zur Haftung und Datenschutzbestimmungen.

- Dann besteht in der Schweiz Impressumspflicht: Auf Verträge, die ausschliesslich durch den Austausch von elektronischer Post oder durch vergleichbare individuelle Kommunikation geschlossen werden.

- Dann besteht in Deutschland Impressumspflicht: Vor allem Verkaufsplattformen wie Online-Shops und Suchmaschinen müssen ein Impressum angeben. Aber auch Accounts in sozialen Netzwerken wie Facebook, Instagram, Twitter und Co. benötigen ein Impressum, wenn das Konto auch gewerblich, beispielsweise für das Bewerben von Produkten oder Stellenanzeigen, genutzt wird.

Disclaimer:

- Übersetzungen: Haftungsausschluss (DE), la clause de non-responsabilité (FR), dichiarazione liberatoria (IT), exoneración de responsabilidad (SP)

- Definition: Ein "Disclaimer" ist eine rechtliche Erklärung oder ein rechtlicher Hinweis, der auf einer Webseite oder in anderen Veröffentlichungen platziert wird, um bestimmte Informationen oder rechtliche Aspekte zu klären und Haftung zu begrenzen. 

AGB:

- Definition: "AGB" steht für "Allgemeine Geschäftsbedingungen". Dies sind rechtliche Dokumente, die die Bedingungen und Regeln festlegen, unter denen ein Unternehmen seine Produkte oder Dienstleistungen an Kunden oder Benutzer anbietet. In den AGB finden sich Informationen über Zahlungsbedingungen, Lieferzeiten, Garantien, Haftungsbeschränkungen, Widerrufs- und Rückgaberechte sowie andere wichtige Vertragsbedingungen.

- Links: 
 * https://www.zalando.ch/zalando-agb/
 * https://www.jelmoli.ch/agb
 * https://shorturl.at/wGNQ8

### Lizenzen

Lizenz Definition: 

- Eine Lizenz ist eine Genehmigung, die von einem Rechteinhaber erteilt wird, um anderen Personen bestimmte Rechte oder Privilegien auf ein geistiges Eigentum, eine Software, ein Produkt oder eine Dienstleistung zu gewähren. Sie legt die Bedingungen fest, unter denen diese Rechte genutzt werden dürfen und kann kostenpflichtig oder kostenfrei sein.

Warum kommt man auf die Idee, Lizenzen zu schaffen?

- Rechtekontrolle: Lizenzen ermöglichen es Rechteinhabern, die Verwendung ihrer Software oder anderer geschützter Inhalte zu kontrollieren. Sie legen die Bedingungen fest, unter denen andere Personen diese Inhalte nutzen dürfen, und bieten rechtliche Sicherheit für beide Seiten.

- Schutz vor Missbrauch: Lizenzen ermöglichen es Rechteinhabern, die Verwendung ihres geistigen Eigentums oder ihrer Produkte einzuschränken und vor unerlaubter Nutzung oder Missbrauch zu schützen.

- Einkommensquelle: Lizenzen können eine Einnahmequelle für Rechteinhaber darstellen. Sie erlauben es, Gebühren oder Lizenzgebühren von Personen oder Unternehmen zu erheben, die die Rechte nutzen möchten.

Worauf gibt es alles Lizenzen ausser für Software?
 
- Musik, Filme, Marken, Literatur, Kunstwerke

Lizenzmodelle:
 
- Open-Source-Lizenz: Bei Open-Source-Software handelt es sich um eine Lizenz, die den Quellcode öffentlich verfügbar macht und es Dritten ermöglicht, die Software zu nutzen, zu ändern und zu verteilen, solange sie die Bedingungen der Lizenz einhalten. z.B Ubuntu

- Proprietäre Lizenz: Bei dieser Art von Lizenz behält der Rechteinhaber die volle Kontrolle über sein geistiges Eigentum und gewährt anderen Personen begrenzte Nutzungsrechte, normalerweise gegen Gebühren oder Lizenzgebühren. z.B Windows

- Musiklizenz: Musiker und Musikverlage gewähren Lizenzen für die Verwendung ihrer Musik in Filmen, Werbung, Radio und anderen Medien.

- Markenlizenz: Markeninhaber können anderen Unternehmen Lizenzen erteilen, um ihre Marken und Logos in Produkten oder Werbekampagnen zu verwenden.

Opensource:
 
- "Open Source" bedeutet, dass die Quellcode-Basis einer Software öffentlich verfügbar ist und von jedermann eingesehen, geändert und weiterverbreitet werden kann, solange dies den Bedingungen der jeweiligen Open-Source-Lizenz entspricht. Open-Source-Software fördert die Zusammenarbeit und den gemeinsamen Zugang zu Software und ermöglicht es der Entwicklergemeinschaft, frei an der Verbesserung und Erweiterung der Software zu arbeiten.
 
- Kann gratis genutzt, verändert & verbreitet werden.

Copyright & Copyleft:

- "Copyleft" ist ein Konzept, das im Zusammenhang mit Open-Source-Software verwendet wird. Es bezieht sich auf bestimmte Arten von Softwarelizenzen, die die Freiheit und das Recht zur Verwendung, Modifikation und Weiterverteilung der Software erläutern.

- "Copyright" ist ein Begriff, der das traditionelle Urheberrecht repräsentiert. Es schützt die Rechte des Urhebers oder Rechteinhabers eines Werkes und ermöglicht es diesem, die Verwendung, Vervielfältigung, Verbreitung und Änderung des Werkes zu kontrollieren.

- Unterschied: Copyright verbietet alles, was im Copyleft erlaubt ist. Copyright ist das Gegenteil von Copyleft.

Welches Lizenz-Modell wird angewendet, wenn man im App-Store eine zu bezahlende App heunterlädt und installiert?

- Proprietäre Lizenz: Weil der App-Entwickler oder Anbieter die volle Kontrolle über die Rechte und Bedingungen der Nutzung der App behält. Als Benutzer erhält man in der Regel eine Lizenz, die es einem erlaubt, die App auf seinem Geräten zu nutzen, jedoch unter bestimmten Bedingungen, die in den Nutzungsbedingungen und der Endbenutzer-Lizenzvereinbarung (EULA) festgelegt sind.

Welches Lizenz-Modell wird angewendet, wenn man im App-Store eine gratis App heunterlädt und installiert?

- Freeware-Lizenz: Diese Art von Lizenz ermöglicht es einem, die App kostenlos zu nutzen, ohne dass man eine direkte Gebühr zahlen müssen.